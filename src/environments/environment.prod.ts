import { HttpHeaders } from "@angular/common/http";

export const environment = {
    production: true,
    //apiURL: 'http://localhost:22222/apiForum/themes',
	apiURL: 'api',
    apiThemeURL: 'themes',
	apiQuestionURL: 'questions',
	apiAnswersURL: 'answers',
    apiAuth: 'http://localhost:22222/OAuthentication/auth',
    forumApi: '',
    httpOptions: {
        headers: new HttpHeaders({
            'Content-Type': 'application/json'
        })
    }
};
