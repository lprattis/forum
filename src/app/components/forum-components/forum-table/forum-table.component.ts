//Angular
import { Component, OnInit, ViewChild, Input, EventEmitter, Output, OnDestroy, ChangeDetectionStrategy, OnChanges } from '@angular/core';
import { MatSort, MatPaginator, MatTableDataSource } from "@angular/material";
import { Router } from "@angular/router";
import { Observable, Subscription, BehaviorSubject } from 'rxjs';
import { SelectionModel } from "@angular/cdk/collections";
//Model
import { Column } from "model/column";
//Services
import { LoginService } from 'services/login.service';
import { User } from 'model/user';

@Component({
    selector: 'app-forum-table',
    templateUrl: './forum-table.component.html',
    styleUrls: ['./forum-table.component.css']
})
export class ForumTableComponent implements OnInit, OnDestroy {
    @Input('data') data: Observable<any>;
    @Input('columns') columns: Column[];
    @Input('sort-header') sortHeader: string;
    @Input('router-link') link: string;
    @Input('comparatorProfile') comparator: Function;
    @Input('entity') entity: Observable<any>;
    @Output() rowSelected = new EventEmitter();

    displayedColumns: string[];
    dataSource = new MatTableDataSource();
    resultsLength: number = 0;
    selection = new SelectionModel(false, []);
    user: User;

    userSubscription: Subscription;
    allowedProfilesSubscription: Subscription;
    entitySubscription: Subscription;
    dataSubscription: Subscription;

    profileAllowedSubject: BehaviorSubject<Boolean>;
    profileAllowedObservable: Observable<Boolean>;
    profileAllowed: Boolean;

    @ViewChild(MatSort) sort: MatSort;
    @ViewChild(MatPaginator) paginator: MatPaginator;

    constructor(
        private router: Router,
        private loginService: LoginService
    ) { 
        this.profileAllowedSubject = new BehaviorSubject<Boolean>(false);
        this.profileAllowedObservable = this.profileAllowedSubject.asObservable();
    }

    ngOnInit() {
        this.displayedColumns = this.columns.map(column => column.name);
        this.entitySubscription = this.entity.subscribe(
            entity => {
                this.userSubscription = this.loginService.currentUser.subscribe(
                    user =>{
                        this.profileAllowedSubject.next(this.comparator(user,entity.leader_id));
                    });
            });  

        this.allowedProfilesSubscription = this.profileAllowedObservable.subscribe(x => {
            this.profileAllowed = x;
            if(this.profileAllowed){
                if (!this.displayedColumns.includes('deleteItem')) 
                    this.displayedColumns.unshift('deleteItem');
            }else{
                if (this.displayedColumns.includes('deleteItem'))
                    this.displayedColumns.shift();
            }
        });

        this.dataSubscription = this.data.subscribe(data => this.dataSource.data = data);
    }

    ngAfterViewInit() {
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
    }

    ngOnDestroy(){
        if(this.userSubscription)
            this.userSubscription.unsubscribe();
        if(this.allowedProfilesSubscription)
            this.allowedProfilesSubscription.unsubscribe();
        if(this.entitySubscription)
            this.entitySubscription.unsubscribe();
        if(this.dataSubscription)
            this.dataSubscription.unsubscribe();
    }

    selectRow(row: any) {        
        if (this.link) {
            this.router.navigate([this.link+'/'+row.id]);
        }
    }

    applyFilter(filterValue: string) {
        filterValue = filterValue.trim();
        filterValue = filterValue.toLowerCase();
        this.dataSource.filter = filterValue;
    }
 
    onSelect(row: any){
        this.selection.toggle(row);
        this.rowSelected.emit(this.selection.selected);
    }
}
