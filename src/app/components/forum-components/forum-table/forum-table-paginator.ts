import { MatPaginatorIntl } from "@angular/material";
import { Injectable } from "@angular/core";

@Injectable()
export class MatPaginatorIntlCro extends MatPaginatorIntl{
    itemsPerPageLabel = 'Elements per pàgina';
    firstPageLabel = 'Principi';
    lastPageLabel = 'Final';
    nextPageLabel = 'Avança';
    previousPageLabel = 'Retrocedeix';
}