import { Component, Input, Output, EventEmitter, OnDestroy } from '@angular/core';
//Model
import { Theme } from "model/theme";
import { LoginService } from 'services/login.service';
import { Subscription } from 'rxjs';
import { User } from 'model/user';

@Component({
    selector: 'app-item-themes',
    templateUrl: './item-themes.component.html',
    styleUrls: ['./item-themes.component.css']
})
export class ItemThemesComponent implements OnDestroy {
    @Output() dialogEvent = new EventEmitter<dataToEmmit>();
    @Input('theme') data: Theme;

    getUserSubscription: Subscription;
    currentUser: User;
    
    constructor(private loginService:LoginService){
        this.getUserSubscription = this.loginService.currentUser.subscribe(x => this.currentUser = x);
    }

    ngOnDestroy(){
        this.getUserSubscription.unsubscribe();
    }

    openEditDialog(){
        var data: dataToEmmit = {
            action: 'edit',
            id_theme: this.data.id
        }
        this.dialogEvent.emit(data);
    }
    openDeleteDialog(){
        var data: dataToEmmit = {
            action: 'delete',
            id_theme: this.data.id
        }
        this.dialogEvent.emit(data);
    }
}

interface dataToEmmit{
    action: String,
    id_theme: number
}
