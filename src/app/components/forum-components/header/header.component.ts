import { Component, OnDestroy, OnInit, HostListener } from '@angular/core';
import { environment } from "environments/environment";
import * as HoldOn from 'assets/HoldOn.min.js';
import { LoginService } from "services/login.service";
import { User } from 'model/user';
import { Subscription } from 'rxjs';
declare var HoldOn: any;
@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.css'],
})

export class HeaderComponent implements OnDestroy {
    currentUser: User;
    currentUserSubscription: Subscription;
    state: String;
    constructor(private loginService:LoginService) {
        this.currentUserSubscription = this.loginService.currentUser
                                            .subscribe(user => this.currentUser = user);
          
    }

    ngOnDestroy() {
        this.currentUserSubscription.unsubscribe();
    }

    @HostListener('window:message', ['$event'])
    onMessage(event: any) {
      this.messageCallback(event);
    }

    openLogin() {
		var leftPosition: number, topPosition: number;
		//Allow for borders.
		leftPosition = (window.screen.width / 2) - ((500 / 2) + 10);
		//Allow for title and status bars.
        topPosition = (window.screen.height / 2) - ((500 / 2) + 50);
        this.state = Math.random().toString(36).substring(7);
        var url: string = environment.apiAuth + '?response_type=id_token%20token' + '&client_id=forum' +
        '&redirect_uri='+ window.location.protocol +'%2F%2F'+ window.location.host + '%2Ftoken'
        +'&scope=openid%20profile&state='+ this.state;
		var loginPopup = window.open(url,
			"_blank",
		   "toolbar=no, scrollbars=1,status=no, dependent=yes, resizable=0, width=" + 500 + ", height=" + 500 + 
		   ", left=" + leftPosition + ", top=" + topPosition + ", titlebar=no, menubar=no, location=no");
		HoldOn.open({
			theme:"sk-cube-grid"
	   	});

		var popupTick = setInterval(function() {
			if (loginPopup.closed) {
			  clearInterval(popupTick);
			  HoldOn.close();
			}
          }, 500);
          
        //   
    }
    
    messageCallback(event: any){
        if(event.origin === 'http://localhost:33333' ){
            this.loginService.login(event.data);
        }
    }

    loginMock(role: string){
        this.loginService.loginMock(role);
        window.location.reload();
    }
    
    logout(){
        this.loginService.logout();
        // swindow.location.reload();
    }

}
