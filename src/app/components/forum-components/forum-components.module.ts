import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
    MatToolbarModule, MatButtonModule,
    MatSortModule, MatTableModule,
    MatCardModule, MatInputModule,
    MatIconModule, MatListModule,
    MatPaginatorModule, MatPaginatorIntl,
    MatChipsModule, MatDialogModule,
    MatCheckboxModule, MatMenuModule
} from "@angular/material";
import { HeaderComponent } from './header/header.component';
import { MessagesComponent } from './messages/messages.component';
import { AppRoutingModule } from "../../app-routing.module";
import { ItemThemesComponent } from './item-themes/item-themes.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import { FooterComponent } from './footer/footer.component';
import { ForumTableComponent } from './forum-table/forum-table.component';
import { MatPaginatorIntlCro } from "./forum-table/forum-table-paginator";

@NgModule({
    imports: [
        CommonModule,
        AppRoutingModule,
        MatToolbarModule,
        MatButtonModule,
        MatSortModule,
        MatTableModule,
        MatCardModule,
        MatIconModule,
        MatPaginatorModule,
        MatFormFieldModule,
        MatInputModule,
        MatCheckboxModule,
        MatMenuModule
    ],
    declarations: [
        HeaderComponent,
        MessagesComponent,
        ItemThemesComponent,
        FooterComponent,
        ForumTableComponent,
    ],
    exports: [
        HeaderComponent,
        MessagesComponent,
        ItemThemesComponent,
        MatButtonModule,
        MatFormFieldModule,
        MatInputModule,
        MatIconModule,
        MatCardModule,
        MatListModule,
        FooterComponent,
        ForumTableComponent,
        MatChipsModule,
        MatDialogModule
    ],
    providers: [
        { provide: MatPaginatorIntl, useClass: MatPaginatorIntlCro }
    ],
    
})
export class ForumComponentsModule { }
