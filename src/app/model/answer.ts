export interface Answer{
    id?: number,
    questionId: number;
    whoId: string;
    whoName: string;
    date: Date;
    text: string;
}