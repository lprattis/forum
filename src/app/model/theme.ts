export interface Theme {
    id?: number;
    category: string;
    leader_id: string;
    leader_name: string;
    title: string;
    description: string;
}