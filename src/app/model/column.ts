export interface Column {
    name: string;
    text: string;
    width?: string;
    dateFormat?: string;
}