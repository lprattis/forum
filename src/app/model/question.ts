export interface Question {
    id?: number;
    themeId: number;
    whoId: string;
    whoName: string;
    date: Date;
    subject: string;
    text: string;
}