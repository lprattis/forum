import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from "@angular/common/http";
import { FormsModule } from '@angular/forms';
import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ReactiveFormsModule } from "@angular/forms";
//Components
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { ForumComponentsModule } from "./components/forum-components/forum-components.module";
import { ThemeMenuComponent } from './theme-menu/theme-menu.component';
import { ThemeDetailComponent } from './theme-detail/theme-detail.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { AnswersComponent } from './answers/answers.component';
import { AddModalComponent } from './theme-menu/add-edit-theme-modal/add-modal.component';
import { AddQuestionModalComponent } from "./theme-detail/add-modal/add-modal.component";
import { AddAnswerModalComponent } from "./answers/add-modal/add-modal.component";
//Services
import { ThemeService } from "services/theme.service";
import { QuestionService } from "services/question.service";
import { MessageService } from 'services/message.service';
import { LoginService } from "services/login.service";
import { InMemoryDataService } from 'services/in-memory-data.service';
import { ShareDataService } from "services/share-data.service";
import { AnswersService } from "services/answers.service";
//Interceptor
import { HttpConfigInterceptor } from "services/jwt.interceptor";


@NgModule({
    declarations: [
        AppComponent,
        ThemeMenuComponent,
        ThemeDetailComponent,
        NotFoundComponent,
        AnswersComponent,
        AddModalComponent,
        AddQuestionModalComponent,
        AddAnswerModalComponent
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        FormsModule,
        ForumComponentsModule,
        HttpClientModule,
        BrowserAnimationsModule,
        ReactiveFormsModule,
        HttpClientInMemoryWebApiModule.forRoot(
            InMemoryDataService, {
                dataEncapsulation: false
            }),
    ],
    bootstrap: [AppComponent],
    providers: [
        ThemeService,
        QuestionService,
        MessageService,
        LoginService,
        ShareDataService,
        AnswersService,
        { provide: HTTP_INTERCEPTORS, useClass: HttpConfigInterceptor, multi: true }
    ],
    entryComponents: [AddModalComponent, AddQuestionModalComponent, AddAnswerModalComponent],
})
export class AppModule { }
