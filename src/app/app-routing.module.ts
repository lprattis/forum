import { NgModule } from '@angular/core';
import { RouterModule, Routes, ExtraOptions } from "@angular/router";
import { ThemeMenuComponent } from "./theme-menu/theme-menu.component";
import { ThemeDetailComponent } from "./theme-detail/theme-detail.component";
import { NotFoundComponent } from "./not-found/not-found.component";
import { AnswersComponent } from "./answers/answers.component";

const routes: Routes = [
  { path: 'temes', component: ThemeMenuComponent },
  { path: 'temes/:id', component: ThemeDetailComponent },
  { path: 'temes/:idTheme/preguntes/:idAnswer', component: AnswersComponent }, 
  { path: 'not-found', component: NotFoundComponent },
  { path: '', redirectTo: '/temes', pathMatch: 'full' },
  { path: '**', component: NotFoundComponent }
]
const options: ExtraOptions =
  {
    //enableTracing:true
  }


@NgModule({
  exports:[
    RouterModule
  ],
  imports:[
    RouterModule.forRoot(routes,options)
  ]
})
export class AppRoutingModule { }