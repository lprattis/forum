import { Component, OnInit, Inject, OnDestroy } from '@angular/core';
import { FormControl, Validators, FormGroup } from "@angular/forms";
import { MatDialogRef } from "@angular/material";
import { MAT_DIALOG_DATA } from '@angular/material';
//Models
import { Answer } from 'model/answer';
//Services
import { AnswersService } from "services/answers.service";
import { User } from 'model/user';
import { LoginService } from 'services/login.service';
import { Subscription } from 'rxjs';

@Component({
    selector: 'app-add-modal',
    templateUrl: './add-modal.component.html',
    styleUrls: ['./add-modal.component.css']
})
export class AddAnswerModalComponent implements OnInit, OnDestroy{
    model: Answer;
    answerForm = new FormGroup({
        text: new FormControl([
            Validators.required
        ])
    });
    mode: string;
    user: User;
    userSubscription: Subscription;

    constructor(
        @Inject(MAT_DIALOG_DATA) public data: any,
        private dialogRef: MatDialogRef<AddAnswerModalComponent>,
        private answerService: AnswersService,
        private loginService: LoginService
    ) {
        this.model = {
            date: new Date(),
            whoId: '',
            whoName: '',
            text: '',
            questionId: this.data.idQuestion
        }
        this.mode = this.data.action;
    }

    ngOnInit(){
        this.userSubscription = this.loginService.currentUser.subscribe(user =>{
            this.model.whoName = user.userName;
            this.model.whoId = user.id;
        })
        if (this.mode === 'delete') {
            this.answerForm.disable();
            this.getAnswer();
        }
    }

    ngOnDestroy(){
        this.userSubscription.unsubscribe();
    }

    onSubmit() {
        this.answerService.addAnswer(this.model)
            .subscribe(answer => this.dialogRef.close(answer));
        //this.dialogRef.close("Modal tancat");
    }
    delete() {
        this.answerService.removeAnswer(this.model.id)
            .subscribe(() => this.dialogRef.close(this.model.id));
    }

    getAnswer(): void {
        this.answerService.getAnswer(this.data.id).subscribe(question => {
            if (question) {
                this.model = question;
            } else {
                this.dialogRef.close('Resposta no trobada');
            }
        });
    }
}


