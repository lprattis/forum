import { Component, OnInit, OnDestroy } from '@angular/core';
import { Location } from '@angular/common';
import { Observable, Subscription, BehaviorSubject } from 'rxjs';
import { Router, ActivatedRoute } from "@angular/router";
import { MatDialog } from "@angular/material";
//Services
import { AnswersService } from "services/answers.service";
import { AddAnswerModalComponent } from "./add-modal/add-modal.component";
import { LoginService } from "services/login.service";
import { ThemeService } from "services/theme.service";
import { QuestionService } from "services/question.service";
//Model
import { Theme } from "model/theme";
import { Question } from "model/question";
import { Column } from "model/column";
import { Answer } from "model/answer";
import { User } from 'model/user';

@Component({
    selector: 'app-answers',
    templateUrl: './answers.component.html',
    styleUrls: ['./answers.component.css']
})
export class AnswersComponent implements OnInit, OnDestroy {
    theme: Theme;
    question: Question;
    selectedAnswer: Answer;
    currentUser: User;
    sortTitle: String;
    columns: Column[];

    idTheme: number;
    idQuestion: number;

    answerSubject: BehaviorSubject<Answer[]>;
    answerObservable: Observable<Answer[]>;
    themeObservable: Observable<Theme>;

    themeSubscription: Subscription;
    answerSubscription: Subscription;
    questionSubscription: Subscription;
    userSubscription: Subscription;
    
    constructor(private answersService: AnswersService, private router: Router,
                private route: ActivatedRoute, private location: Location,
                private dialog: MatDialog, private loginService: LoginService, 
                private questionService: QuestionService, private themeService: ThemeService) {
        this.sortTitle = "name";
        this.columns = [
            {
                name: 'whoName',
                text: 'Autor',
                width: '10'
            }, {
                name: 'text',
                text: 'Pregunta'
            }, {
                name: 'date',
                text: 'Data creació',
                dateFormat: 'HH:mm dd/MM/yy',
                width: '20'
            }
        ];
        this.idTheme = +this.route.snapshot.paramMap.get('idTheme');
        this.idQuestion = +this.route.snapshot.paramMap.get('idAnswer');
        this.userSubscription = this.loginService.currentUser.subscribe(user => this.currentUser = user);
        this.themeObservable = this.themeService.getTheme(this.idTheme);
        this.answerSubject = new BehaviorSubject<Answer[]>([]);
        this.answerObservable = this.answerSubject.asObservable();
    }

    userPermissionCallback(user: User, idLiderTheme: string ): boolean{
        if (user && user.role) {
            switch (user.role) {
                case 'admin':
                    return true;
                case 'lider':
                    return user.id === idLiderTheme;        
                default:
                    return false;
            }
        }else{
            return false;
        }
    }

    ngOnInit() {
        this.themeSubscription = this.themeObservable.subscribe(
            theme => {
                if (theme) {
                    this.theme = theme;
                }else{
                    this.router.navigate(['/not-found']);
                }
            },
            () => this.router.navigate(['/not-found']));

        this.questionSubscription = this.questionService.getQuestion(this.idQuestion).subscribe(
            question => this.question = question,
            () => this.router.navigate(['not-found'])
        );
        this.getAnswers();
    }

    ngOnDestroy(){
        if(this.userSubscription)
            this.userSubscription.unsubscribe();
        if(this.themeSubscription)
            this.themeSubscription.unsubscribe();
        if(this.questionSubscription)
            this.questionSubscription.unsubscribe();
        if(this.answerSubscription)
            this.answerSubscription.unsubscribe();
    }

    goBack(): void {
        this.location.back();
    }

    getAnswers(): void {
        this.answerSubscription = this.answersService.getAnswers(this.idQuestion).subscribe(
            answers => {
                this.answerSubject.next(answers);
            }
        );
    }
    answerSelected(answer: Answer[]){
        this.selectedAnswer = answer.length == 0 ? null : answer[0];
    }
    openAddDialog() {
		const addModal = this.dialog.open(AddAnswerModalComponent, {
			closeOnNavigation: true,
			width: '80%',
			data: {
				idQuestion: this.question.id
			}
		});

		addModal.afterClosed().subscribe(result => {
			if (result) {
                this.getAnswers();
			}
		});
    }
    openRemoveDialog() {
		const addModal = this.dialog.open(AddAnswerModalComponent, {
			closeOnNavigation: true,
			width: '80%',
			data: {
                id: this.selectedAnswer.id,
				action:'delete'
			}
		});

		addModal.afterClosed().subscribe(result => {
			if (result) {
                this.getAnswers();
                this.selectedAnswer = null;
			}
		});
    }
}
