//Angular
import { Component, OnInit, OnDestroy } from '@angular/core';
import { MatDialog } from '@angular/material';
//Services
import { ThemeService } from "services/theme.service";
import { LoginService } from 'services/login.service';

//Model
import { Theme } from "model/theme";

//Modal
import { AddModalComponent } from "./add-edit-theme-modal/add-modal.component";
import { Subscription } from 'rxjs';
import { User } from 'model/user';

@Component({
	selector: 'app-theme-menu',
	templateUrl: './theme-menu.component.html',
	styleUrls: ['./theme-menu.component.css']
})
export class ThemeMenuComponent implements OnInit, OnDestroy {
	data: Theme[];
	themeSubscription: Subscription;
	currentUserSubscription: Subscription;
	currentUser: User;
	constructor(
		private themeService: ThemeService,
		public dialog: MatDialog,
		private loginService: LoginService
	) { 
		this.currentUserSubscription = this.loginService.currentUser.subscribe(user => this.currentUser = user);
	}
	ngOnInit() {
		this.getThemes();
	}

	ngOnDestroy(){
		this.themeSubscription.unsubscribe();
		this.currentUserSubscription.unsubscribe();
	}

	getThemes(){
		this.themeSubscription = this.themeService.getThemes().subscribe(Themes => this.data = Themes);
	}

	openAddDialog() {
		const addModal = this.dialog.open(AddModalComponent, {
			closeOnNavigation: true,
			width: '80%',
			data: {
				action:'add'
			}
		});

		addModal.afterClosed().subscribe(result => {
			if (result) {
				this.getThemes();
			}
		});
	}
	openEditDeleteDialog(event: any) {
		const editModal = this.dialog.open(AddModalComponent, {
			closeOnNavigation: true,
			width: '80%',
			data: {
				id: event.id_theme,
				action: event.action
			}
		})
		editModal.afterClosed().subscribe(result => {
			if (result) {
				this.getThemes()
			}
		});
	}
}