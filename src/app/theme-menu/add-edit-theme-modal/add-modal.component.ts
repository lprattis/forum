import { Component, OnInit, Inject } from '@angular/core';
import { FormControl, Validators, FormGroup } from "@angular/forms";
import { MatDialogRef } from "@angular/material";
import { MAT_DIALOG_DATA } from '@angular/material';
//Services
import { ThemeService } from "services/theme.service";
//Model
import { Theme } from "model/theme";

@Component({
    selector: 'app-add-modal',
    templateUrl: './add-modal.component.html',
    styleUrls: ['./add-modal.component.css']
})

export class AddModalComponent implements OnInit {
    model: Theme;
    mode: String;
    themeForm = new FormGroup({
        title: new FormControl([
            Validators.required
        ]),
        description: new FormControl([
            Validators.required
        ]),
        category: new FormControl([
            Validators.required
        ])
    });
    constructor(
        @Inject(MAT_DIALOG_DATA) public data: any,
        private dialogRef: MatDialogRef<AddModalComponent>,
        private themeService: ThemeService
    ) {
        this.model = {
            title: '',
            description: '',
            category: '',
            leader_id: '2',
            leader_name: 'Anna'
        }
        this.mode = this.data.action;
    }
    ngOnInit() {
        if (this.mode !== 'add') {
            this.getTheme();
        }
        if (this.mode === 'delete') {
            this.themeForm.disable();
        }
    }
    edit() {
        this.themeService.editTheme(this.model)
            .subscribe(() => this.dialogRef.close(this.model.id));
    }

    delete() {
        this.themeService.removeTheme(this.model.id)
            .subscribe(() => this.dialogRef.close(this.model.id));
    }

    onSubmit() {
        this.themeService.addTheme(this.model)
            .subscribe(theme => this.dialogRef.close(theme));
        //this.dialogRef.close("Modal tancat");
    }

    getTheme(): void {
        this.themeService.getTheme(this.data.id).subscribe(theme => {
            if (theme) {
                this.model = theme;
            }else{
                this.dialogRef.close('Tema no trobat');
            }
        });
    }
}
