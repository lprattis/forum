import { Component, OnInit, Inject, OnDestroy } from '@angular/core';
import { FormControl, Validators, FormGroup } from "@angular/forms";
import { MatDialogRef } from "@angular/material";
import { MAT_DIALOG_DATA } from '@angular/material';
//Models
import { Question } from "model/question";
//Services
import { QuestionService } from "services/question.service";
import { User } from 'model/user';
import { Subscription } from 'rxjs';
import { LoginService } from 'services/login.service';
@Component({
    selector: 'app-add-modal',
    templateUrl: './add-modal.component.html',
    styleUrls: ['./add-modal.component.css']
})
export class AddQuestionModalComponent implements OnInit, OnDestroy {
    model: Question;
    mode: String;
    user: User;
    userSubscription: Subscription;
    questionForm = new FormGroup({
        subject: new FormControl([
            Validators.required
        ]),
        text: new FormControl([
            Validators.required
        ])
    });
    constructor(
        @Inject(MAT_DIALOG_DATA) public data: any,
        private dialogRef: MatDialogRef<AddQuestionModalComponent>,
        private questionService: QuestionService,
        private loginService: LoginService
    ) {
        this.model = {
            subject : '',
            date: new Date(),
            whoId: '',
            whoName: '',
            text: '',
            themeId: 0
        }
        this.mode = this.data.action;
    }
    ngOnInit() {
        this.userSubscription = this.loginService.currentUser.subscribe(user => {
            this.model.whoId = user.id;
            this.model.whoName = user.userName;
        });
        if (this.mode === 'delete') {
            this.questionForm.disable();
            this.getQuestion();
        }
    }

    ngOnDestroy(){
        this.userSubscription.unsubscribe();
    }

    delete() {
        this.questionService.removeQuestion(this.model.id)
            .subscribe(() => this.dialogRef.close(this.model.id));
    }

    onSubmit() {
        this.questionService.addQuestion(this.model)
            .subscribe(questio => this.dialogRef.close(questio));
        //this.dialogRef.close("Modal tancat");
    }

    getQuestion(): void {
        this.questionService.getQuestion(this.data.id).subscribe(question => {
            if (question) {
                this.model = question;
            } else {
                this.dialogRef.close('Questió no trobada');
            }
        });
    }
}


