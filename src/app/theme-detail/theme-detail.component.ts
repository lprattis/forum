import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { Observable, Subscription, BehaviorSubject } from 'rxjs';
import { tap, map } from 'rxjs/operators';
import { MatDialog } from "@angular/material";
import { AddQuestionModalComponent } from "./add-modal/add-modal.component";
//Services
import { ThemeService } from 'services/theme.service';
import { QuestionService } from "services/question.service";
//Model
import { Theme } from "model/theme";
import { Question } from "model/question";
import { Column } from "model/column";
import { LoginService } from 'services/login.service';
import { User } from 'model/user';

@Component({
    selector: 'app-theme-detail',
    templateUrl: './theme-detail.component.html',
    styleUrls: ['./theme-detail.component.css']
})


export class ThemeDetailComponent implements OnInit, OnDestroy {
    theme: Theme;
    selectedQuestion: Question;

    // questions: Observable<Question[]>;
    QuestionSubject: BehaviorSubject<Question[]>;
    QuestionObservable: Observable<Question[]>;
    themeObservable: Observable<Theme>;

    columns: Column[] = [
        {
            name: 'whoName',
            text: 'Autor',
            width: '10'
        }, {
            name: 'text',
            text: 'Pregunta'
        }, {
            name: 'date',
            text: 'Data creació',
            dateFormat: 'HH:mm dd/MM/yy',
            width: '20'
        }];
    
    currentUser: User;

    userSubscription: Subscription;
    themeSubscription: Subscription;
    questionSubscription: Subscription;

    routerLink: String;
    idTheme: number;
    idLider: string;
    constructor(
        private route: ActivatedRoute,
        private themeService: ThemeService,
        private questionService: QuestionService,
        private location: Location,
        public dialog: MatDialog,
        private loginService: LoginService) {
            this.idTheme = +this.route.snapshot.paramMap.get('id');
            this.userSubscription = this.loginService.currentUser.subscribe(x => this.currentUser = x);
            this.themeObservable = this.themeService.getTheme(this.idTheme);
            this.QuestionSubject = new BehaviorSubject<Question[]>([]);
            this.QuestionObservable = this.QuestionSubject.asObservable();
        }

    ngOnInit() {
        this.routerLink = '/temes/'+this.idTheme+'/preguntes/';
        this.themeSubscription = this.themeObservable.subscribe(theme => this.theme = theme);
        this.getQuestions();
    }

    ngOnDestroy(){
        this.userSubscription.unsubscribe();
        this.themeSubscription.unsubscribe();
        this.questionSubscription.unsubscribe();
    }

    goBack(): void {
        this.location.back();
    }

    userPermissionCallback(user: User, idLiderTheme: string ): boolean{
        if (user && user.role) {
            switch (user.role) {
                case 'admin':
                    return true;
                case 'lider':
                    return user.id === idLiderTheme;        
                default:
                    return false;
            }
        }else{
            return false;
        }
    }

    getQuestions(): void {
        this.questionSubscription = this.questionService.getQuestions(this.idTheme as any as string).subscribe(
            questions => {
                this.QuestionSubject.next(questions);
            }
        );
    }

    openAddDialog() {
		const addModal = this.dialog.open(AddQuestionModalComponent, {
			closeOnNavigation: true,
			width: '80%',
			data: {
				action:'add'
			}
		});

		addModal.afterClosed().subscribe(result => {
			if (result) {
                this.getQuestions();
			}
		});
    }
    
    openRemoveDialog(){        
        const addModal = this.dialog.open(AddQuestionModalComponent, {
			closeOnNavigation: true,
			width: '80%',
			data: {
                id: this.selectedQuestion.id,
				action:'delete'
			}
		});

		addModal.afterClosed().subscribe(result => {
			if (result) {
                this.getQuestions();
                this.selectedQuestion = null;
			}
		});
    }
    questionSelected(question: Question[]){
        this.selectedQuestion = question.length == 0 ? null : question[0];
    }
}
