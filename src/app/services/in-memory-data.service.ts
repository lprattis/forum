import { InMemoryDbService } from 'angular-in-memory-web-api';
//Model
import { Theme } from "../model/theme";
import { Question } from "model/question";
import { Answer } from "model/answer";

export class InMemoryDataService implements InMemoryDbService {
  createDb(){
    const questions: Question[] = [{
      id:1,
      themeId: 1,
      whoId: '1',
      whoName: 'Usuari 1',
      date: new Date('Mon, 04 Dec 2018 09:30:00'),
      subject: 'Materia 1',
      text: 'Every Angular application has at least one component, the root component that connects a component hierarchy with the page DOM?'
    },{
      id:2,
      themeId: 2,
      whoId: '2',
      whoName: 'Usuari 2',
      date: new Date('Mon, 25 Dec 1995 13:30:00'),
      subject: 'Materia 1',
      text: 'Each component defines a class that contains application data and logic, and is associated with an HTML template that defines a view to be displayed in a target environment?'
    },{
      id:3,
      themeId: 4,
      whoId: '1',
      whoName: 'Usuari 1',
      date: new Date('Mon, 25 Dec 2001 13:30:00'),
      subject: 'Materia 2',
      text: 'The @Component decorator identifies the class immediately below it as a component, and provides the template and related component-specific metadata?'
    },{
      id:4,
      themeId: 5,
      whoId: '1',
      whoName: 'Usuari 1',
      date: new Date('Mon, 25 Dec 2001 13:30:25'),
      subject: 'Materia 3',
      text: 'For data or logic that is not associated with a specific view, and that you want to share across components, you create a service class?'
    },{
      id:5,
      themeId: 3,
      whoId: '1',
      whoName: 'Usuari 1',
      date: new Date('Mon, 25 Dec 2010 18:30:00'),
      subject: 'Materia 1',
      text: 'They don\'t fetch data from the server, validate user input, or log directly to the console; they delegate such tasks to services?'
    },{
      id:6,
      themeId: 6,
      whoId: '2',
      whoName: 'Usuari 2',
      date: new Date('Mon, 25 Dec 1995 13:30:00'),
      subject: 'Materia 1',
      text: 'Each component defines a class that contains application data and logic, and is associated with an HTML template that defines a view to be displayed in a target environment?'
    },{
      id:7,
      themeId: 2,
      whoId: '1',
      whoName: 'Usuari 1',
      date: new Date('Mon, 25 Dec 2001 13:30:00'),
      subject: 'Materia 2',
      text: 'The @Component decorator identifies the class immediately below it as a component, and provides the template and related component-specific metadata?'
    },{
      id:8,
      themeId: 2,
      whoId: '1',
      whoName: 'Usuari 1',
      date: new Date('Mon, 25 Dec 2001 13:30:25'),
      subject: 'Materia 3',
      text: 'For data or logic that is not associated with a specific view, and that you want to share across components, you create a service class?'
    }];
    const themes : Theme[] = [{
      id:1,
      category:"categoria 1",
      leader_id:"2",
      leader_name:"lider 1",
      title:"Tema 1",
      description: `Angular defines the NgModule, which differs from and complements the JavaScript (ES2015) module. `
    },{
      id:2,
      category:"categoria 2",
      leader_id:"4",
      leader_name:"lider 2",
      title:"Tema 2",
      description: `An NgModule declares a compilation context for a set of components that is dedicated to an application domain, a workflow, or a closely related set of capabilities `
    },{
      id:3,
      category:"categoria 3",
      leader_id:"3",
      leader_name:"lider 3",
      title:"Tema 3",
      description: `An NgModule can associate its components with related code, such as services, to form functional units.`
    },{
      id:4,
      category:"categoria 2",
      leader_id:"2",
      leader_name:"lider 2",
      title:"Tema 4",
      description: `Every Angular app has a root module, conventionally named AppModule, which provides the bootstrap mechanism that launches the application`
    },{
      id:5,
      category:"categoria 3",
      leader_id:"2",
      leader_name:"lider 3",
      title:"Tema 5",
      description: `Like JavaScript modules, NgModules can import functionality from other NgModules, and allow their own functionality to be exported and used by other NgModules`
    },{
      id:6,
      category:"categoria 4",
      leader_id:"4",
      leader_name:"lider 4",
      title:"Tema 6",
      description: `Organizing your code into distinct functional modules helps in managing development of complex applications, and in designing for reusability`
    },{
      id:7,
      category:"categoria 4",
      leader_id:"4",
      leader_name:"lider 4",
      title:"Tema 7",
      description: `Every Angular app has a root module, conventionally named AppModule, which provides the bootstrap mechanism that launches the application`
    }];
    const answers: Answer[] = [{
      id:1,
      questionId: 1,
      whoId:"1",
      whoName :"Usuari 1",
      date: new Date('Mon, 04 Dec 2017 09:30:00'),
      text: `Reflection makes it possible to inspect classes, interfaces, fields and methods at runtime, without knowing the names of the classes, methods etc. at compile time. It is also possible to instantiate new objects, invoke methods and get/set field values using reflection.`
    },{
      id:2,
      questionId: 1,
      whoId:"2",
      whoName :"Usuari 2",
      date: new Date('Mon, 04 Dec 2018 09:30:00'),
      text: `Good Real time example is MyEclipse IDE. if you put your mouse pointer over any class name it reflect the class name and with some information. This is also one type of reflection using reflection api.`
    },{
      id:3,
      questionId: 1,
      whoId:"3",
      whoName :"Usuari 3",
      date: new Date('Mon, 10 Dec 2018 09:30:00'),
      text: `That's why every object who wants custom equality should implement an equals method. Eventually, you'll want to put the objects into a set, or use them as a hash index, then you'll have to implement equals anyway. Other languages do it differently, but Java uses equals. You should stick to the conventions of your language.`
    },{
      id:4,
      questionId: 2,
      whoId:"1",
      whoName :"Usuari 1",
      date: new Date('Mon, 10 Dec 2018 15:30:00'),
      text: `Also, "boilerplate" code, if put into the correct class, is pretty hard to screw up. Reflection adds additional complexity, meaning additional chances for bugs. In your method, for example, two objects are considered equal if one returns null for a certain field and the other doesn't. What if one of your getters returns one of your objects, without an appropriate equals? Your if (!object1.equals(object2)) will fail. Also making it bug prone is the fact that reflection is rarely used, so programmers aren't as familiar with its gotchas.`
    },{
      id:5,
      questionId: 2,
      whoId:"2",
      whoName :"Usuari 2",
      date: new Date('Mon, 04 Dec 2015 09:30:00'),
      text: `Overusing of reflection probably depends on the language used. Here you are using Java. In that case, reflection should be used with care because often it is only a workaround for bad design.

      So, you are comparing different classes, this is a perfect problem for method overriding. Note that instances of two different classes should never be considered equal. You can compare for equality only if you have instances of the same class. See `
    },{
      id:6,
      questionId: 2,
      whoId:"3",
      whoName :"Usuari 3",
      date: new Date('Mon, 04 Dec 1995 09:30:00'),
      text: `We use Eclipse RCP as our application platform, which is OSGi based (Equinox). Our application makes usage of both the Eclipse plugins as well as pure OSGi bundles. In our case we have services that are both local to the application as well as remote (via Spring Remoting) to Java EE servers.`
    },{
      id:7,
      questionId: 3,
      whoId:"1",
      whoName :"Usuari 1",
      date: new Date('Mon, 04 Dec 2001 09:30:00'),
      text: `If you envision ever wanting to run different components on separate hardware, then writing using web services (REST/SOAP/etc) makes sense. But there's a performance penalty in both sending bits from one process or network to another,and serializing/marshalling that data so it can be sent, and doing the opposite on the other side. If you don't ever think that will happen, then don't use web services.`
    },{
      id:8,
      questionId: 3,
      whoId:"2",
      whoName :"Usuari 2",
      date: new Date('Mon, 04 Dec 2018 09:30:00'),
      text: `Basically Stateful means that server stores information about the client and uses that information over a series of requests. So performing one request is dependant upon the state of some other request (e.g. previous). Implementing this is possible with http protocols.`
    },{
      id:9,
      questionId: 3,
      whoId:"3",
      whoName :"Usuari 3",
      date: new Date('Mon, 04 Dec 2018 09:31:00'),
      text: `So you can have stateful or stateless SOAP - it's only a matter of how you design it.

      Also please note that comparing SOAP and REST is not really correct. The first one is basically Protocol (or at least it's trying to be) and REST is just a architecture pattern/style.
      
      I know this is not exactly answering your question but please take a look at this link: SOAP vs REST (differences) It's extremely well written and can help you understand those technologies a bit better.`
    },
    ]
    return {themes, questions, answers};
  } 
}
