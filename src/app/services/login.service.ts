import { Injectable } from '@angular/core';
import { of } from 'rxjs/observable/of';
import { Observable, BehaviorSubject } from "rxjs";

//Service
import { MessageService } from "services/message.service";

//Model
import { User } from "model/user";

@Injectable()
export class LoginService {
    // private loginURL: string = environment.apiAuth;
    private currentUserSubject: BehaviorSubject<User>;
    public currentUser: Observable<User>;
    constructor(
        private messageService: MessageService
    ) { 
        this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(sessionStorage.getItem('currentUser')));
        this.currentUser = this.currentUserSubject.asObservable();
    }

    public get currentUserValue(): User {
        return this.currentUserSubject.value;
    }

    loginMock(role: string){
        var user: User;
        switch (role) {
            case 'admin':
                user = {
                    id: '1',
                    role: 'admin',
                    userName: 'Joan'
                }
                break;
            case 'lider':
                user = {
                    id: '2',
                    role: 'lider',
                    userName: 'Anna'
                }
                break;
            case 'basic':
                user = {
                    id: '3',
                    role: 'basic',
                    userName: 'Susana'
                }
                break;
            default: break;
        }
        sessionStorage.setItem('currentUser', JSON.stringify(user));
        this.currentUserSubject.next(user);
    }

    login(user: any){
        var userlogged: User = {
            id: user.id,
            userName: user.userName,
            role: user.role
        }
        sessionStorage.setItem('token', user.token);
        sessionStorage.setItem('currentUser', JSON.stringify(userlogged));
        this.currentUserSubject.next(user);
        //window.location.reload();
    }

    getToken(): string {
        return sessionStorage.getItem('token');
    }

    logout(): void {
        this.currentUserSubject.next(null);
        if (sessionStorage.getItem('token')) {
            sessionStorage.removeItem('token');
        }
        sessionStorage.setItem('currentUser',null);
    }
    
    isAuthenticated(): boolean{
        return this.currentUserSubject.value !== null;
    }

    getUserData(): User {
        return this.currentUserSubject.value;
    }

    getUserRole(): string {
        return this.currentUserSubject.value.role;
    }

    getUserName(): string {
        return this.currentUserSubject.value.userName;
    }

    private handleAuthError<T>(result?: T) {
        return (error: any): Observable<T> => {
            console.error(error);
            this.showMessage(`Error d'autenticació: ${error.message}`, 'danger');
            return of(result as T);
        }
    }

    private showMessage(message: string, level?: string) {
        this.messageService.add(message, level);
    }
}
