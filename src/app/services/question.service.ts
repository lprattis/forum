import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable } from "rxjs";
import { catchError, tap } from "rxjs/operators";
import { of } from 'rxjs/observable/of';
//Model
import { Question } from "model/question";
import { environment } from "environments/environment";
//Service
import { MessageService } from "./message.service";

@Injectable()
export class QuestionService {
    constructor(
        private http: HttpClient,
        private messageService: MessageService
    ) { }

    getQuestions(idTheme: string): Observable<Question[]> {
        return this.http.get<Question[]>(environment.apiURL +'/'+ environment.apiQuestionURL+'?themeId='+idTheme)
        .pipe(
            //tap(questions => console.log('Crida http: ', questions)),
            catchError(this.handleError([]))
        );
    }

    getQuestion(idQuestion: Number): Observable<Question> {
        return this.http.get<Question>(environment.apiURL +'/'+ environment.apiQuestionURL + '/' + idQuestion)
        .pipe(
            //tap(question => console.log('Crida http: ', question)),
            catchError(this.handleError<Question>())
        );
    }

    addQuestion(newQuestion: Question): Observable<Question>{
        return this.http.post<Question>(environment.apiURL +'/'+ environment.apiQuestionURL, newQuestion, environment.httpOptions)
        .pipe(
            // tap(question => console.info('questio creada: ',question)),
            catchError(this.handleError<Question>())
        );
    }

    removeQuestion(idQuestion: number):Observable <{}>{
        var url :string = environment.apiURL +'/'+ environment.apiQuestionURL + '/' + idQuestion;
        return this.http.delete(url, environment.httpOptions)
        .pipe(
            catchError(this.handleError('deleteQuestion'))
        )
    }


    private showMessage(message: string, level?: string) {
        this.messageService.add(message, level);
    }
    private handleError<T>(result?: T) {
        return (error: any): Observable<T> => {
            console.error(error);
            this.showMessage('Error '+ error.status+ ': Qüestió ' +error.statusText+'.','danger');
            return of(result as T);
        }
    }
}
