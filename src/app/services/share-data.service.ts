import { Injectable } from '@angular/core';
import { previousData } from "model/previousData";

@Injectable()
export class ShareDataService {

  data: previousData;
  constructor() { }

  getSharedData() {
    return this.data;
  }

  setSharedData(data: previousData) {
    this.data = data;
  }
}
