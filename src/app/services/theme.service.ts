import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable } from "rxjs";
import { catchError, tap } from "rxjs/operators";
import { of } from 'rxjs/observable/of';
//Model
import { Theme } from "model/theme";
import { environment } from "environments/environment";
//Services
import { MessageService } from "./message.service";

@Injectable()
export class ThemeService {

    private apiThemeURL = 'api/Temes';

    constructor(
        private messageService: MessageService,
        private http: HttpClient
    ) { }


    getThemes(): Observable<Theme[]> {
        return this.http.get<Theme[]>(environment.apiURL+'/'+environment.apiThemeURL)
        .pipe(
            //tap(Temes => console.log('Crida http: ', Temes)),
            catchError(this.handleError([]))
        );
    }

    getTheme(id: number): Observable<Theme> {
        return this.http.get<Theme>(environment.apiURL+'/'+environment.apiThemeURL + '/' + id)
        .pipe(
            //tap(theme => console.log('Theme -> ', theme)),
            catchError(this.handleError<Theme>())
        )
    }

    addTheme(newTheme: Theme):Observable<Theme>{
        return this.http.post<Theme>(environment.apiURL+'/'+environment.apiThemeURL, newTheme, environment.httpOptions)
        .pipe(
            //tap(id_Theme => console.info('Tema creat. ID: ',id_Theme)),
            catchError(this.handleError<Theme>())
        )
    }

    removeTheme(idtheme: number):Observable <{}>{
        var url :string = environment.apiURL+'/'+this.apiThemeURL + '/' + idtheme;
        return this.http.delete(url, environment.httpOptions)
        .pipe(
            catchError(this.handleError('delete Question'))
        )
    }

    editTheme(theme: Theme): Observable<Theme>{
        return this.http.put<Theme>(environment.apiURL+'/'+this.apiThemeURL, theme, environment.httpOptions)
        .pipe(
            catchError(this.handleError<Theme>())
        )
    }
    //this.showMessage('Tema afegit correctament', 'success')

    //level: success, info, warning, danger 
    private showMessage(message: string, level?: string) {
        this.messageService.add(message, level);
    }

    private handleError<T>(result?: T) {
        return (error: any): Observable<T> => {
            console.error(error);
            this.showMessage('Error '+ error.status+ ': Tema ' +error.statusText+'.','danger');
            return of(result as T);
        }
    }
}