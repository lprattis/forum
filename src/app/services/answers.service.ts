import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { catchError, tap } from "rxjs/operators";
import { of } from 'rxjs/observable/of';
//Model
import { Answer } from "model/answer";
import { environment } from "environments/environment";
//Service
import { MessageService } from "./message.service";

@Injectable()
export class AnswersService {
	constructor(
		private http: HttpClient,
		private messageService: MessageService
	) { }

	getAnswers(questionId: number): Observable<Answer[]> {
		return this.http.get<Answer[]>(environment.apiURL+'/'+environment.apiAnswersURL+'?questionId='+questionId).pipe(
			// tap(answers => console.log('Crida http: ', answers)),
			catchError(this.handleError([]))
		);
	}

	getAnswer(idAnswer: Number): Observable<Answer> {
        return this.http.get<Answer>(environment.apiURL+'/'+environment.apiAnswersURL + '/' + idAnswer).pipe(
            // tap(answer => console.log('Crida http: ', answer)),
            catchError(this.handleError<Answer>())
        );
    }

	addAnswer(answer: Answer): Observable<Answer> {
		return this.http.post<Answer>(environment.apiURL+'/'+environment.apiAnswersURL, answer, environment.httpOptions).pipe(
			// tap(answers => console.log('Crida http: ', answers)),
			catchError(this.handleError<Answer>())
		);
	}
	removeAnswer(idAnswer: number):Observable <{}>{
        var url :string = environment.apiURL +'/'+ environment.apiAnswersURL + '/' + idAnswer;
        return this.http.delete(url, environment.httpOptions).pipe(
            catchError(this.handleError('deleteQuestion'))
        )
	}
	

	private showMessage(message: string, level?: string) {
		this.messageService.add(message, level);
	}
	private handleError<T>(result?: T) {
		return (error: any): Observable<T> => {
			console.error(error);
			this.showMessage('Error '+ error.status+ ': Resposta ' +error.statusText+'.','danger');
			return of(result as T);
		}
	}

}
