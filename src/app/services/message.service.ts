import { Injectable } from '@angular/core';

interface message{
  message: string,
  level: string
}

@Injectable()
export class MessageService {
  messages: message[];
  constructor() { 
    this.messages = []
  }
  
  add( message: string, level: string = 'info'){
    this.messages.push({
        message: message,
        level: 'alert-'+level
    });
    // console.log(this.messages);
  }

  clear(){
    this.messages = [];
  }
}
